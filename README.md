# Gretchen

My take on the walking Robot **Gretchen**.

## Original Links

* <https://berlin-united.org/project-gretchen.html>
* <https://github.com/aibrainag/Gretchen.git>
* <https://www.youtube.com/watch?v=ubMeLkMhT9Y>
* <https://humanoid.robocup.org/wp-content/uploads/2020-Gretchen-VROHOW.pdf>

### Servo software

* <https://github.com/suprememachines/sensorimotor>
* <https://github.com/suprememachines/libsensorimotor>

## Todo

* [ ] Servo Electronics + Firmware
* [ ] CAD Model Rhino 3D
* [ ] Walking Simulation
* [ ] Real Walking Robot
* [ ] Walking Dojo

## Servo firmware

```bash
arm-none-eabi-gdb

target remote localhost:1337
file target/thumbv7m-none-eabi/debug/servo
monitor reset
continue
```
